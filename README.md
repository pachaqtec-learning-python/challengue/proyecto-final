# Backend - Proyecto Final

![Build Status](https://travis-ci.org/joemccann/dillinger.svg?branch=master)

### <span style="color:green">**Introducción**</span>.
---
Se plantea desarrollar una aplicación web responsive para ver la información de todo el TEAM Empresarial, así como chatear de manera privada entre ellos, y dando la posibilidad que cada uno de ellos pueda editar su Perfil. **Se pide que desarrolles los servicios backend necesarios que atiendan la necesidad planteada.** Para ello debes poner en practica todos lo aprendido en estas ultimas 16 semanas.

El proyecto final debera ser realizado durante las **semanas 16 y 18**, en donde tendras un acompañamiento en asesorías con tus profesores. Tendras 11.5 horas que debes organizar con ellos para lograr tu objetivo.

---

## 👇 **Indicaciones**

- El planteamiento debe enfocarse en cumplir con la descripción del reto.
- Se deberán armar grupos de 02 integrantes por equipo.
- Solo deberán tomar los ejercicios desarrollados en clases como modelos a seguir.
- Se debe asignar un líder de equipo por Grupo, el cual se encargará de aceptar los merge request.
- Cada alumno expondrá la parte que desarrolló en el proyecto

## 📋 **Evaluación**

Estos son los puntos que se van a evaluar y son puntos importantes a tomar antes de empezar a desarrollar

- Uso de git para control de versiones. Uso de ramas (master, develop), asi como uso de comandos push, pull, merge, tag, stash, reset, checkout, entre otros.
- Se revisara código para ver avance progresivo para evitar copiar y pegar ejemplos de apis resueltas en el curso.
- Se revisara la cantidad de commits, y el mensaje claro que debe tener cada uno de ellos. [Revisar info](https://trello.com/c/wklzmTNw)
- Los servicios se deben desplegar en un servidor de producción.
- La presentación del proyecto se hará en una url pública.
- El trabajo debe ser resultado de una participación colaborativa y activa de todo el grupo. Se evaluará en base a los merge request o commits aportados.

## 🛠️ **Herramientas Develop**

- Trello para organizar tareas.
- Ambiente Cloud Azure o AWS.
- Teams para comunicación semanal de avances y resolución de inconvenientes.
- Uso de GitHub o GitLab. (Repositorio Privado).
- Postman para uso y pruebas de APIS.
- Uso de SQLLite para pruebas de base de datos relacionales.


## 📝 **Pautas**

- Crear backlog en trello. Historias de Usuario y criterios de aceptación.
- Crear estructura kanban para cada team. [Revisar info](https://trello.com/c/1dGVsXpT)
- Definir arquitectura de trabajo (BD, CDN, etc.)
- Definir prioridades MOSCOW [Revisar Info](https://trello.com/c/4Vt368NF)
- Definir Flujo de trabajo GIT [Revisar info](https://trello.com/c/ds8qW1IE)

## 💻 **Servicios Rest API**

Debemos realizar las siguiente funcionalidades para cumplir con el reto:

> 🔴 **TOMAR EN CUENTA...!!!**
>
> 1. Se debe aplicar pruebas unitarias para todos los servicios expuesto para eventos positivos y negativos
> 2. Se debe trabajar con postman en trabajo colaborativo con uso de environment.


- API - Validar Usuario
  > 🔔 *Criterios - Formulario de Login*
  - Validar ingreso en base a correo y password
  - Aplicar concepto de Json Token

- API - Crear Usuario
  > 🔔 *Criterios - Formulario de Registro*
  - Se debe guardar el año de nacimiento.
  - El número movil debera guardar el formato+519XXXXXXXX. Debe validar el ingreso de solo números del 0 al 9.
  - Debe haber una lista de las sedes habilitadas.
  - Nombre y apellidos por separado.

  ![alt text](img/formulario_registro.png "Formulario Registro")

- API Editar Usuario
  > 🔔 *Criterios - Formulario Editar Perfil*
  - Debe poder editar sus datos de registro.
  - Debe poder subir o cambiar su foto y tener una por defecto.
  - Validaciones en los datos de ingresos.

  ![alt text](img/formulario_perfil.png "Formulario Perfil")

- API List ALL Usuario
  > 🔔 *Criterios - View Principal*
  - Devolver una lista de todos los usuarios el cual puede tener especialización y un orden especifico.
  - Devolver lista de usuario de acuerdo al campo de busqueda que seria el nombre. Ejemplo si ingreso 3 palabras buscaria todos los nombres que contengan dichas palabras.

  ![alt text](img/formulario_inicio.png "Formulario Inicio")

- API Ver Usuario
  > 🔔 *Criterios - View Detalle de Usuario*
  - Devolver los datos de acuerdo al userID.
  - Devolver referencia de imagen en CDN.

  ![alt text](img/formulario_detalle_user.png "Formulario Detalle Usuario")

- API Cambios de Estado
  > 🔔 *Criterios - Cambios de estado por usuario*
  - Deberas devolver la lista de usuarios conectados y desconectados.
  - Deberas decidir en mostrar información de ultima conexión activa.
  - Debera existir un api para cambiar el estado del agente.
  - Se deberan definir los estados "Conectado, Ocupado, Ausente, No Disponible, Desconectado"

  ![alt text](img/formulario_estados.png "Formulario Estados")


## 💻 **Temas Adicionales**

Podemos hacer uso de varios servicios de AWS o AZURE para realizar temas adicionales que suben al desarrollo como tal. Por ejemplo:

- Podemos aplicar servicios sass como analisis de sentimientos en base al perfil ingresado por el usuario.
- Podemos agregar tags en donde indiques que lenguajes de programación conoces como "NodeJS, Python, PHP, JAVA, Etc"
- Podemos validar que la imagen subida no sea de temas sexuales por ejemplo o que aparezca el rostro.


---
## **👍😀 TE DESEAMOS SUERTE...!!!!**
